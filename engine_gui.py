#!/bin/python
# ENGINE PARA DAMAS
# autores: DSPCompas

# Imports dos módulos usados ao logo do programa 
import numpy as np
from numpy import random as rand
from copy import deepcopy
from anytree import AnyNode, LevelOrderIter
import pygame
import numpy as np
import matplotlib.pyplot as plt
# Trabalho desenvolvido para a disciplina IA006
# dada no segundo semestre de 2023 pelo professor
# Romis Attux com título "Introdução ao xadrez com-
# putacional". Como o jogo de xadrez é difícil de-
# mais pra vencermos em 2 meses, pensamos que um
# programa "assistente" para o jogo de damas pudes-
# se ser uma alternativa mais humilde (kkkkkkkk) p/
# um trabalho final. Dessa forma, seguimos a seguin-
# te organização para o jogo:
#
#
#   O RINGUE: O espaço das matrizes 8x8
#
#  |  0 -1  0 -1  0 -1  0 -1 |
#  | -1  0 -1  0 -1  0 -1  0 |
#  |  0 -1  0 -1  0 -1  0 -1 |
#  |  0  0  0  0  0  0  0  0 |
#  |  0  0  0  0  0  0  0  0 |
#  |  1  0  1  0  1  0  1  0 |       <——————————————+
#  |  0  1  0  1  0  1  0  1 |                      |
#  |  1  0  1  0  1  0  1  0 |                      |
#                                                   |
#   OS LUTADORES:                                   |
#                                                   |
#    1 = 'o'        Obs.: A representação de duas   |
#   -1 = 'x'              peças pelos caracteres    |
#    0 = ' '              'x' e 'o' para facilitar  |
#                         o processo de depuração   |
#                         pelo terminal.            |
#                                                   |
# Com isso, criou-se uma funçãozinha chamada        |
# `print_tab` com o intuito de se imprimir o ta-    |
# buleiro de forma amigável. A matriz acima, >——————+
# portanto, transforma-se em:                       |
#                                                   |
#       +—+—+—+—+—+—+—+—+                           |
#       | |x| |x| |x| |x|                           |
#       +—+—+—+—+—+—+—+—+                           |
#       |x| |x| |x| |x| |                           |
#       +—+—+—+—+—+—+—+—+                           |
#       | |x| |x| |x| |x|                           |
#       +—+—+—+—+—+—+—+—+                           |
#       | | | | | | | | |                           |
#       +—+—+—+—+—+—+—+—+   >———————————————————————+
#       | | | | | | | | |
#       +—+—+—+—+—+—+—+—+
#       |o| |o| |o| |o| |
#       +—+—+—+—+—+—+—+—+
#       | |o| |o| |o| |o|
#       +—+—+—+—+—+—+—+—+
#       |o| |o| |o| |o| |
#       +—+—+—+—+—+—+—+—+

### PYGAME TEST - Define variáveis e funções para tabuleiro e peças
# Inicializa a fonte 
pygame.font.init()
# Define as dimensões da janela e do tabuleiro
WIDTH, HEIGHT = 400, 450 
ROWS, COLS = 8, 8
SQUARE_SIZE = WIDTH // COLS

# Define as cores para as casas escuras e claras
GRAY = (127, 127, 127) # cinza
WHITE = (255, 255, 255) # branco

# define score text
font = pygame.font.Font('freesansbold.ttf', 32)

# Inicializa o Pygame
pygame.init()

# Cria uma janela
window = pygame.display.set_mode((WIDTH, HEIGHT))

# Define um título para a janela
pygame.display.set_caption("Jogo da Dama do DSPCOM")

# Função para desenhar o tabuleiro
def draw_board():
    for row in range(ROWS):
        for col in range(COLS):
            # Seleciona a cor do quadrado
            if (row + col) % 2 == 0:
                color = WHITE
            else:
                color = GRAY

            # Cria no tabuleiro a casa em questão
            pygame.draw.rect(
                window,
                color,
                (col * SQUARE_SIZE,
                 row * SQUARE_SIZE,
                 SQUARE_SIZE,
                 SQUARE_SIZE)
            )

# Função para preencher o tabuleiro com as peças
def draw_pieces(board):
    for row in range(ROWS):
        for col in range(COLS):
            if board[row][col] == -1:
                # black
                pygame.draw.circle(
                    window,
                    (0, 0, 0),
                    (col * SQUARE_SIZE + SQUARE_SIZE // 2,
                     row * SQUARE_SIZE + SQUARE_SIZE // 2),
                    SQUARE_SIZE // 2 - 10
                )
            elif board[row][col] == 1:
                # white
                pygame.draw.circle(
                    window,
                    (255, 255, 255),
                    (col * SQUARE_SIZE + SQUARE_SIZE // 2,
                     row * SQUARE_SIZE + SQUARE_SIZE // 2),
                    SQUARE_SIZE // 2 - 10
                )
            elif board[row][col] == -2 :
                # navy blue - dama das pretas
                pygame.draw.circle(
                    window,
                    (0, 0, 100),
                    (col * SQUARE_SIZE + SQUARE_SIZE // 2,
                     row * SQUARE_SIZE + SQUARE_SIZE // 2),
                    SQUARE_SIZE // 2 - 10
                )
            elif board[row][col] == 2 :
                # light gray - dama das brancas
                pygame.draw.circle(
                    window,
                    (200, 200, 200),
                    (col * SQUARE_SIZE + SQUARE_SIZE // 2,
                     row * SQUARE_SIZE + SQUARE_SIZE // 2),
                    SQUARE_SIZE // 2 - 10
                )



# Fç de avaliação de tabuleiro válido --> num sei
# se isso vai prestar pra alguma coisa, mas tem que
# começar d'algum lugar...
def valid_conf (P):
    '''Avalia se a configuração de interesse é vá-
       lida ou não. Isso significa ter pelo menos
       16 peças oponentes em campo.'''

    # Inicializa var. de retorno como verdadeira
    ret = True

    # Conta número de peças do nosso campo
    num_self = len(np.where(P == 1.)[0])

    # Conta número de peças adversárias
    num_adv  = len(np.where(P == -1.)[0])

    # Verifica se o número de peças existentes
    # condiz para um jogo de damas
    if num_self > 16 or num_adv > 16:
        ret = False

    # Retorna var. booleana de controle
    return ret

def calc_score (P): #, P_draw):
    '''Função básica de cálculo de escore para uma
       determinada configuração de tabuleiro. Essa
       métrica pode ser avaliada de diversas formas
       levando em consideração diversos fatores.

       Relação de inputs:
           P --> Matriz de configuração avaliada

       Relação de saídas:
           score --> Escalar contendo o escore cal-
                     culado'''

    # ------------------------------------------
    # Insere termos relativos às pos. no tab.
    # cálculos inspirados em no link abaixo
    #                                  vvv
    # https://github.com/Gualor/checkers-minimax
    # ------------------------------------------
    
    # Inicia variável de escore
    score = 0.

    # Avalia posições das peças 'o'
    idx_v, idy_v   = np.where(P > 0.)
    numero_pecas_o = len(idx_v)

    # Termo associado a qtd de peças 'o' em P
    score += len(idx_v) * 100.

    # Termo associado a qtd de damas
    score += len(P > 1.) * 500.

    # Itera por cada peça 'o'
    for idx, idy in zip(idx_v, idy_v):
        if np.abs(P[idx, idy]) < 2.:
            # Soma termo de proximidade com parede
            score += (1 - (0.5 / abs(idy - 5.5))) * 50.

            # Soma termo de proximidade com a borda
            score += idx * 10.

    # Avalia posições das peças 'x'
    idx_v, idy_v   = np.where(P < 0.)
    numero_pecas_x = len(idx_v)

    # Termo associado a qtd de peças 'x' em P
    score -= len(idx_v) * 100.

    # Termo associado a qtd de damas
    score -= len(P < -1.) * 500.

    # Itera por cada peça 'x'
    for idx, idy in zip(idx_v, idy_v):
        if np.abs(P[idx, idy]) < 2.:
            # Soma termo de proximidade com parede
            score += -1. * (1 - (0.5 / abs(idy - 5.5))) * 50.

            # Soma termo de proximidade com a borda
            score += -1. * (7. - idx) * 10.

    # Retorna o score calculado
    
    # CHECA SE HOUVE VENCEDOR. CASO POSITVO RETORNA UM SCORE INFINITO
    if (numero_pecas_o > 0) and (numero_pecas_x == 0):
        score = np.inf
    elif (numero_pecas_o == 0) and (numero_pecas_x > 0):
        score = - np.inf

    # CHECA EMPATE
    #mask_draw = (P == P_draw)
    #if np.all(mask_draw):
    #    score = 0

    return score

def eval_viz (P, ii, jj, turn):
    '''Função de avaliação da vizinhança de uma
       determinada peça, baseada em turno e posi-
       ção no tabuleiro.

       Relação de inputs:
           P  --> Matriz de configuração avaliada
           ii --> Índice de linha da posição ava-
                  liada
           jj --> Índice de coluna da posição ava-
                  liada
           turn --> Flag de turno

       Relação de saídas:
           viz_idx --> índices de linha das vizinhan-
                       ças válida
           viz_idy --> índices de coluna das vizinhan-
                       ças válida'''

    # Inicializa lista de vizinhanças de casas vazias
    viz_idx = []
    viz_idy = []

    # Inicializa lista de vizinhanças p/ casas de captura
    viz_idx_cap = []
    viz_idy_cap = []

    # Gera índice idx a partir do turno. Obs.: uma peça
    # normal no jogo de damas pode mover para trás s.se.
    # ela puder comer a pecinha de trás. Sendo assim, a
    # depender do turno, verifica-se a fileira traseira,
    # p/ ver se tem possibilidade de captura.
    if turn > 0:
        idx_v = [ii-1, ii+1]        # Coluna traseira sem-
    else:                           # no fim do vetor.
        idx_v = [ii+1, ii-1]

    # Verifica a partir do turno qual o sentido do movi-
    # mento da dama
    for lin, idx in enumerate(idx_v):
        for idy in [jj-1, jj+1]:
            # Verifica se a pos. avaliada está no tabuleiro
            if idy < 8 and idy >= 0 and idx < 8 and idx >= 0:
                # Caso de casa vazia
                if P[idx, idy] == 0.:
                    # Só pode avançar para uma casa va-
                    # zia se ela estiver na coluna da
                    # frente ou se a peça for uma dama.
                    # Isso implica em só mexer pra trás
                    # pra comer...
                    if lin == 0 or np.abs(P[ii][jj]) == 2.:
                        viz_idx.append(idx)
                        viz_idy.append(idy)

                # Caso de possibilidade de se comer
                elif np.sign(P[idx, idy]) != np.sign(turn):
                    off_x = ii  - idx
                    off_y = jj  - idy
                    iii   = idx - off_x
                    jjj   = idy - off_y
                    # Verifica se a peça oponente está
                    # protegida pela borda
                    if iii < 8 and iii >= 0 and jjj < 8 and jjj >= 0:
                        # Verifica possibilidade de casa
                        # p/ ficar após o ataque
                        if P[iii, jjj] == 0.:
                            viz_idx_cap.append(idx)
                            viz_idy_cap.append(idy)

    # Verifica se houve ou não captura e retorna a lista
    # apropriada
    if len(viz_idx_cap) > 0:
        return viz_idx_cap, viz_idy_cap
    else:
        return viz_idx, viz_idy

def list_confs (P, turn=1.0):
    '''Função de listagem de possíveis jogadas dada
       uma configuração de tabuleiro. Essa rotina de-
       ve elencar as possíveis jogadas (a depender da
       vez em uma rodada) para uma dada configuração.

       Relação de inputs:
           P    --> Matriz de configuração avaliada 
           turn --> Variável de controle de jogada
       
       Relação de saídas:
           ret_lst --> Lista de configurações de pos-
                       síveis jogadas'''

    # Identifica índices das peças do jogador da ro-
    # dada.
    if turn > 0.0:
        idx, idy = np.where(P >= turn)
    else:
        idx, idy = np.where(P <= turn)

    # Declara lista de retorno
    ret_lst = []

    # Função auxiliar para verificar capturas múltiplas e coroação
    def check_multiple_captures(P, ii, jj, turn, path=[]):
        # Lista de retorno é compartilhada entre as chamadas recursivas
        nonlocal ret_lst
        # Identifica índices da vizinhança da peça
        viz_idx, viz_idy = eval_viz(P, ii, jj, turn)
        # Recupera a peça que irá se mover
        piece = P[ii, jj]
        # Inicializa flag que indica se houve capturas
        captures = False

        # Itera pela vizinhança de cada peça
        for vx, vy in zip(viz_idx, viz_idy):
            # Verifica se a vizinhança é uma casa
            # vazia
            if P[vx, vy] != 0.:
                captures = True
                tmp_conf = P.copy()
                tmp_conf[vx, vy] = 0.
                tmp_conf[ii, jj] = 0.
                off_x = ii - vx
                off_y = jj - vy
                new_ii = vx - off_x
                new_jj = vy - off_y

                # Verifica coroação
                if P[ii, jj] == 1. and new_ii == 0:
                    tmp_conf[new_ii, new_jj] = 2.
                elif P[ii, jj] == -1. and new_ii == 7:
                    tmp_conf[new_ii, new_jj] = -2.
                else:
                    tmp_conf[new_ii, new_jj] = piece

                # Verifica capturas subsequentes
                check_multiple_captures(tmp_conf, new_ii, new_jj, turn, path + [tmp_conf])

        if not captures and path:
            # Sem mais capturas possíveis, adiciona a configuração final à lista
            ret_lst.append(path[-1])

    for ii, jj in zip(idx, idy):
        check_multiple_captures(P, ii, jj, turn)

    if not ret_lst:
        # Se não há movimentos de captura disponíveis, adiciona movimentos regulares
        for ii, jj in zip(idx, idy):
            viz_idx, viz_idy = eval_viz(P, ii, jj, turn)
            piece = P[ii, jj]

            for vx, vy in zip(viz_idx, viz_idy):
                tmp_conf = P.copy()
                tmp_conf[vx, vy] = piece
                tmp_conf[ii, jj] = 0.

                # Verifica se há peça na última coluna
                # ou se houve formação de dama após captura
                if turn > 0 and vx == 0:
                    tmp_conf[vx, vy] = 2.
                if turn < 0 and vx == 7:
                    tmp_conf[vx, vy] = -2.

                # Insere a confguração na lista
                ret_lst.append(tmp_conf)

    # Retorna lista de configurações de próxima jogada
    return ret_lst

def alpha_beta_prun (node, d, turn=1.0, alpha=-1*np.infty, beta=np.infty):
    '''Função responsável pela determinação
       do escore via alfa-beta pruning. Se
       tudo der certo, vai otimizar o custo
       da construção da árvore.

       Relação de inputs:
           node  --> Nó da árvore que será
                     avaliado
           d     --> Profundidade da árvore
           turn  --> Vez do jogador (indicação
                     de maximização)
           alpha --> Margem inferior
           beta  --> Margem superior'''

    # Encerra avaliação se folha ou "folha provisória"
    if node.is_leaf or d == 0:
        # Atualiza valor do escore em caso de "folha
        # provisória" (não é uma configuração final
        # em termos de profundidade).
        if node.score is None:
            node.score = calc_score(node.config)

        return node.score

    # Cria var. de pesos de filha
    vals = []

    # Cria var. temporária p/ armazenamento de info
    # pretérita
    tmp_val = None

    # Faz a avaliação para dos valores de alfa, beta e
    # o escore para cada iteração de acordo com o turno,
    # ou seja, se é ou não maximização
    if turn == 1.0:
        val = -1 * np.infty

        # Itera por cada conf. filha
        for child in node.children:
            # Para otmizar a recusrsão, verifica se valor de
            # poda já foi calculado para o nó
            if 'ab_prun' not in child.__dict__.keys():
                tmp_val = alpha_beta_prun(child, d - 1, -turn, alpha, beta)
                child.ab_prun = tmp_val
            else:
                tmp_val = child.ab_prun

            # Atualiza o escore de forma recursiva
            node.score = np.max([
                val,
                tmp_val
            ])

            # Caso de valor >= beta, nem avalia...
            if node.score >= beta:
                break

            # Atualiza valor de alfa na estrutura e var. livre
            node.alpha = np.max([alpha, node.score])
            alpha      = node.alpha

            # Adiciona elemento em vals
            vals.append(node.score)

        # Detecta o índice ótimo a partir dos escores
        if len(vals) > 0:
            node.opt_idx = np.argmax(vals)
        else:
            node.opt_idx = 0

    # Caso de jogada minimizadora
    else:
        val = np.infty

        # Itera por cada conf. filha
        for child in node.children:
            # Para otmizar a recusrsão, verifica se valor de
            # poda já foi calculado para o nó
            if 'ab_prun' not in child.__dict__.keys():
                tmp_val = alpha_beta_prun(child, d - 1, -turn, alpha, beta)
                child.ab_prun = tmp_val
            else:
                tmp_val = child.ab_prun

            # Atualiza o escore de forma recursiva
            node.score = np.min([
                val,
                tmp_val
            ])

            # Caso de valor <= alfa, nem avalia...
            if node.score <= alpha:
                break

            # Atualiza valor de alfa na estrutura e var. livre
            node.beta = np.min([beta, node.score])
            beta      = node.beta

            # Adiciona escore em vals
            vals.append(node.score)

        # Detecta o índice ótimo a partir dos escores
        if len(vals) > 0:
            node.opt_idx = np.argmin(vals)
        else:
            node.opt_idx = 0

    return node.score

def alpha_beta_tree (T, turn=1., depth=5):
    '''Função de construção de árvore eficiente.
       Nesta rotina está implementado o algorit-
       mo de construção de árvore já implemen-
       tada com a poda alfa-beta. Codificação
       fortemente inspirada no pseudo código
       da página:

           https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning

       Relação de inputs:
           T     --> Configuração de input
           turn  --> Variável de indicação de vez
           depth --> Profundidade da árvore cal-
                     culada

       Relação de saídas:
           root  --> Nó de raiz da árvore computada'''

    # ... Até agora tudo muito parecido com a constru-
    # ção normal de árvore ...

    # Cria var. de identificador global p/ cada nó
    id = 0

    # Cria var. de profundidade
    d = depth

    # Variável de "segurança"
    seg = 10_000

    # Declaração do nó de raiz
    root = AnyNode(
        id     = str(id),
        parent = None,
        config = T,
        turn   = turn,
        score  = None, #score,
        alpha  = -1 * np.infty,
        beta   = np.infty
    )

    # Fç recursiva interna para construção da árvore
    def build_tree_rec (node, turn, depth):
        '''Fç recursiva de construção da árvore. Essa
           função é responsável pela avaliação de todas
           as configurações filhas (para um dado turno)
           e, a partir disso, percorre a árvore.'''

        # Necessário para acessar var. fora do escopo
        nonlocal id

        # para conseguir operacionalizar o alfa-beta
        nonlocal root

        # para conseguir operacionalizar o alfa-beta
        nonlocal  d

        # para controle de demora na recursão
        nonlocal  seg

        # Verifica se a profundidade máxima foi atin-
        # gida
        if depth == 0:
            node.score = calc_score(node.config)

            # Num ligo pro output.... mas faz a poda
            # alfa-beta -> Aqui nois considera quais
            # serão os nós de interesse A partir da
            # ^determinação^ dos limites de um inter-
            # valo de interesse para a avaliação da
            # sub-árvore com escore ∈ [α, β].
            _ = alpha_beta_prun(root, d, turn=root.turn)
            return

        # Verifica se a busca é terminada por fim de
        # jogo --> Atingida configuração de vitória.
        tmp = calc_score(node.config)
        if np.abs(tmp) == np.inf:
            node.score = tmp
            _ = alpha_beta_prun(root, d, turn=root.turn)
            return

        # Verifica se nó já existe na árvore
        if not node.is_leaf:
            # Adquire profundidade das folhas
            # associadas ao nó
            tmp_d = node.leaves[0].depth - node.depth

            # Anda por cada nó das folhas e re-
            # aliza a recursão
            for leaf in node.leaves:
                build_tree_rec(leaf, -1 * turn, depth - tmp_d)
                return

        # Adquire nós filhos
        confs = list_confs(node.config, turn=turn)

        # Itera por cada filho e constroi nó relativo a ele
        for conf in confs:
            # Incrementa id único
            id = id + 1

            # Cria nó com configuração filha
            tmp = AnyNode(
                id     = str(id),
                parent = node,
                config = deepcopy(conf),
                turn   = -1 * turn,
                score  = None,
                alpha  = -1 * np.infty,
                beta   = np.infty
            )

        # Atualiza o contador de segurança
        seg -= len(node.children)

        # Faz o passo de recursão para cada configuração
        for n in node.children:
            # Verifica condiçao de alpha-beta
            if (n.score is None) or n.score > node.alpha and n.score < node.beta:
                # Encerra o loop pelo parâmetro de segurança
                if seg <= 0:
                    break

                # Faz a chamada recursiva só em caso seguro
                build_tree_rec(n, -1 * turn, depth - 1)

        # Encerra função recursiva
        return

    # Inicia a recursão a partir do nó de raiz
    build_tree_rec(
        root,
        turn,
        depth - 1,
    )

    # Retorna o nó de raiz da árvore computada
    return root

def print_tab (T):
    '''Função de impressão mais agradável no terminal das
       disposições dos tabuleiros.

       Relação de inputs:
            T --> Tabuleiro a ser impresso.'''

    # Imprime string de limite de tabuleiro
    print('+—+—+—+—+—+—+—+—+')
    # Itera por cada casa do tabuleiro
    for ii in range(8):
        f_str = '|'

        # Seleciona qual caractere será impresso com
        # base no elemento da matriz tabuleiro.
        #
        # Obs.: Pattern matching é uma propriedade
        #       bem recente em Python... Se n me en-
        #       gano isso só virou parte da lingua-
        #       gem mesmo na versão 3.10. Se alguém
        #       tentar rodar e n funfar, tem q tro-
        #       car isso por uma porrada de 'if',
        #       'elif', 'else' e Cpa. ltda.
        # for jj in range(8):
        #     match T[ii, jj]:
        #         case 1.:
        #             char = 'o'
        #         case -1.:
        #             char = 'x'
        #         case 2.:
        #             char = '0'
        #         case -2.:
        #             char = 'X'
        #         case _:
        #             char = ' '

        for jj in range(8):
            val = T[ii, jj]
            if val == 1.:
                char = 'o'
            elif val ==  -1.:
                char = 'x'
            elif val ==  2.:
                char = '0'
            elif val == -2.:
                char = 'X'
            else :
                char = ' '

            # Adiciona caractere na linha do tabuleiro
            f_str += '%s|' % (char)

        # Imprime a string formatada de linha
        print(f_str)
        print('+—+—+—+—+—+—+—+—+')

scores_list = []
depth_list = []


def play_checkers (mixed_strat=False):
    # Configuração problemática --> Ver oq rola aqui...
    #P = np.array([[ 0., -1.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [-1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [ 0., -1.,  0., -1.,  0.,  0.,  0., -1.],
    #              [-1.,  0.,  0.,  0.,  1.,  0.,  0.,  0.],
    #              [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [ 0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [ 0.,  0.,  0.,  0., -2.,  0., -2.,  0.]])
    # Configuração inicial de jogo
    P = np.array([[ 0., -1.,  0., -1.,  0., -1.,  0., -1.],
                  [-1.,  0., -1.,  0., -1.,  0., -1.,  0.],
                  [ 0., -1.,  0., -1.,  0., -1.,  0., -1.],
                  [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
                  [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
                  [ 1.,  0.,  1.,  0.,  1.,  0.,  1.,  0.],
                  [ 0.,  1.,  0.,  1.,  0.,  1.,  0.,  1.],
                  [ 1.,  0.,  1.,  0.,  1.,  0.,  1.,  0.]])

    #P = np.array([[ 0., 0.,  0., 0.,  0., 0.,  0., 0.],
    #              [ 0.,  0., 0.,  0., 0.,  0., 0.,  0.],
    #              [ 0., 0.,  0., 0.,  0., 0.,  0., 0.],
    #              [ 0.,  -1.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
    #              [ -1.,  0.,  0.,  -1.,  0.,  0.,  0.,  0.],
    #              [ 0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.],
    #              [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.]])

    # Declara variável de turno
    t = -1.

    # Imprime configuração original
    print('Será analisada a seguinte configuração:')
    print()
    print_tab(P)
    print()
    print('Com listagem para o turno: ',
          'x' if t == -1. else 'o')
    #input('Para continuar, aperte enter.')

    # Profundidade
    d = 50

    # Testa fç de construção de árvore p/ a fç avaliada
    root = alpha_beta_tree(P, 1., depth=d)
    #root = build_tree(P, 1., depth=d) 

    # Declara nó auxiliar
    node = deepcopy(root)

    loop          = ''
    lista_jogadas = [node.config]
    turno_atual   = 0
    rodada        = 0

    # create a text surface object, on which text is drawn on it.
    a = 0
    b = 0
    # Função principal do jogo
    running = True
    clock = pygame.time.Clock()


    while loop != 'q':

        while running:
            #a = node.score
            text = font.render("Score: {0:.0f} Depth: {1:.0f}".format(a, b), True, GRAY, WHITE)
            # create a rectangular object for the text surface object
            textRect = text.get_rect()
            # set the center of the rectangular object.
            textRect.center = (WIDTH // 2, HEIGHT - 20)
            window.fill((255, 255, 255))
            window.blit(text,textRect)
            draw_board()
            draw_pieces(node.config)
            pygame.display.update()
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:

                        # Imprime configuração, escore e turno da rodada
                        print_tab(node.config)
                        print('Vez: ', 'x' if node.turn == -1. else 'o')
                        print('Escore: ', node.score)
                        print('Rodada: ', rodada)

                        a = node.score
                        b = node.depth
                        scores_list.append(a)
                        depth_list.append(b)
                        print(scores_list)


                        rodada += 1
                        if not node.parent is None:
                            print('alfa e beta: (%f, %f)'%(node.parent.alpha, node.parent.beta))
                        #print('argumento ótimo: ', node.opt_idx)
                        #loop = input('Tecle <enter> p/ continuar e "q" p/ sair: ')
                        loop = 'r'

                        # TESTE
                        n = alpha_beta_tree(node.config, node.turn, depth=d)

                        # CHECA SE HOUVE VENCEDOR
                        numero_pecas_o = len(np.where(node.config > 0.)[0])
                        numero_pecas_x = len(np.where(node.config < 0.)[0])
                        print(f"Numero de pecas O: {numero_pecas_o}")
                        print(f"Numero de pecas x: {numero_pecas_x}")

                        if numero_pecas_x == 0:
                            print("Vitoria de O")
                            return
                        elif numero_pecas_o == 0:
                            print("Vitoria de X")
                            return
                        

                        # CHECA SE HOUVE EMPATE

                        turno_atual = 0
                        if len(lista_jogadas) > 5:
                            mask_draw = (node.config == lista_jogadas[-5])
                            #print(mask_draw)
                            if np.all(mask_draw):
                                print("EMPATE")
                                return

                        # Define o nó em que será processada a informação
                        tmp = deepcopy(node) if not node.is_leaf else deepcopy(n)

                        # Define forma de jogo a partir da estratégia (se mis-
                        # ta ou pura).
                        if not mixed_strat:
                            # Estratégia pura (determinística)
                            if node.turn == 1.:
                                #idx = np.random.randint(len(tmp.children))
                                #node = deepcopy(tmp.children[idx])
                                node = deepcopy(tmp.children[tmp.opt_idx])
                            else:
                                #node = deepcopy(tmp.children[tmp.opt_idx])
                                idx = np.random.randint(len(tmp.children))
                                node = deepcopy(tmp.children[idx])
                        else:
                            # Estratégia mista (estocástica) com probabilidades
                            # proporcionais aos escores.
                            p_vec = np.array([nn.score for nn in tmp.children])
                            p_vec[p_vec == np.inf]  = 1000.
                            p_vec[p_vec == -np.inf] = -1000.
                            p_vec[p_vec == np.nan]  = 0.

                            p_vec -= np.min(p_vec)
                            p_vec += 1.
                            p_vec /= np.sum(p_vec)

                            idx    = rand.choice(len(p_vec), p=p_vec)
                            node   = deepcopy(tmp.children[idx])

                        lista_jogadas.append(node.config)

                        window.fill((255, 255, 255))
                        window.blit(text,textRect)
                        draw_board()
                        draw_pieces(node.config)
                        pygame.display.update()
                        clock.tick(60)
        pygame.quit()

if __name__ == '__main__':
    play_checkers(mixed_strat=False)
    # Lista todos os nós da árvore
    #node_lst = list(LevelOrderIter(root))

    # Armazena número de nós da árvore
    #num_nodes = len(node_lst)

    ## Itera das folhas até a raiz
    #for ii, node in enumerate(reversed(node_lst)):
    #    # Imprime configuração, escore e turno da rodada
    #    print('Nó de saída: ', num_nodes - ii, ' de ', num_nodes)
    #    print_tab(node.config)
    #    print('Vez: ', 'x' if t == -1. else 'o')
    #    print('Escore: ', node.score)
    #    print('Profundidade: ', node.depth)
    #    print()
    # Normalize scores_list
    # Plot scores_list
    plt.figure(figsize=(10, 5))
    plt.plot(scores_list[:-1])
    plt.title("Scores List")
    plt.xlabel("Iteration")
    plt.ylabel("Score")
    plt.show()

    # Plot depth_list
    plt.figure(figsize=(10, 5))
    plt.plot(depth_list[:-1])
    plt.title("Depth List")
    plt.xlabel("Iteration")
    plt.ylabel("Depth")
    plt.show()


