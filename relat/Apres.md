---
title: Análise sobre o Jogo de Damas
subtitle: Funcionamento da Engine e do sistema desenvolvido
date: 2023
language: portuguese
author:
	- João Rabello Alvim
	- João Pedro Pagnan
affiliation: Introdução ao Xadrez Computacional - IA006
theme: AnnArbor
endnote: true
toc: true
toc-depth: 3
---

# Introdução

## Motivação

- A exploração de jogos é um tema recorrente na humanidade

- Determinados padrões, movimentos e percepções, em jogos são considerados **inteligência**

- O advento da computação permitiu uma nova relação com jogos

## Motivação

- Para a disciplina IA006 estudamos em detalhes o **xadrez computacional**

- Campo riquíssimo em métodos, algoritmos e heurísticas

- Muitos dos conhecimentos podem ser reaproveitados

# Sistema desenvolvido

## Sistema desenvolvido

### Arquitetura geral

- Sistema baseado na proposta de Shannon para o jogo de xadrez
- Baseado na **busca em árvore** por meio do algoritmo *minimax*
- Por conta da profundidade da árvore total, é necessário a implementação de uma **função de avaliação**

## Sistema desenvolvido

### Arquitetura geral - Tabuleiro

- Matriz inteira de tamanho $8 \times 8$
- Peças brancas representeadas por números **positivos**
- Peças pretas representeadas por números **negativos**
- Damas: peças com módulo igual a 2

## Sistema desenvolvido

### Arquitetura geral - Movimentos possíveis

- Peças para **diagonais vazias**
- peões só andam para casas vazias **à frente**
- **Captura**: podem-se realizar **capturas múltiplas** ou **para trás**
- Damas podem andar para frente ou para trás

## Sistema desenvolvido

### Construção da árvore

- Enumeram-se todas as possibilidades de lance
- Um nó pai é aquele sobre o qual serão avaliadas as posições
- Os nós filhos são aqueles derivados dessa posição

## Sistema desenvolvido

### Construção da árvore - Enumeração de lances
```python
def list_confs (P, turn=1.0):
    # Identifica índices das peças do jogador da ro-
    # dada.
    if turn > 0.0:
        idx, idy = np.where(P >= turn)
    else:
        idx, idy = np.where(P <= turn)

    # Declara lista de retorno
    ret_lst = []
```

## Sistema desenvolvido

### Construção da árvore - Enumeração de lances
```python
    if not ret_lst:
        for ii, jj in zip(idx, idy):
            viz_idx, viz_idy = eval_viz(p, ii, jj, turn)
            piece = p[ii, jj]
            for vx, vy in zip(viz_idx, viz_idy):
                tmp_conf = p.copy()
                tmp_conf[vx, vy] = piece
                tmp_conf[ii, jj] = 0.
                if turn > 0 and vx == 0:
                    tmp_conf[vx, vy] = 2.
                if turn < 0 and vx == 7:
                    tmp_conf[vx, vy] = -2.
                ret_lst.append(tmp_conf)

    return ret_lst
```

## Sistema desenvolvido

### Construção da árvore - Enumeração de lances

- Foi desenvolvida a função `eval_viz` para avaliar a vizinhança de cada peça
- São observadas as possibilidades de **casas vazias** e possíveis **capturas**
- O movimento de peões e damas é distinto
- Para seleção das casas de interesse no tabuleiro, é usado o módulo `NumPy`

## Sistema desenvolvido

### Construção da árvore - Criação da estrutura
```python
    def build_tree_rec (node, turn, depth):
        nonlocal id
        nonlocal root
        nonlocal  d
        nonlocal  seg
        if depth == 0:
            node.score = calc_score(node.config)
            _ = alpha_beta_prun(root, d, turn=root.turn)
            return
        tmp = calc_score(node.config)
        if np.abs(tmp) == np.inf:
            node.score = tmp
            _ = alpha_beta_prun(root, d, turn=root.turn)
            return
```

## Sistema desenvolvido

### Construção da árvore - Criação da estrutura
```python
        if not node.is_leaf:
            tmp_d = node.leaves[0].depth - node.depth
            for leaf in node.leaves:
                build_tree_rec(leaf, -1 * turn,
			depth - tmp_d)
                return

        confs = list_confs(node.config, turn=turn)
        for conf in confs:
            id = id + 1
            tmp = AnyNode(...)
```

## Sistema desenvolvido

### Construção da árvore - Criação da estrutura
```python
        seg -= len(node.children)

        for n in node.children:
            if (n.score is None) or n.score > node.alpha
	    and n.score < node.beta:
                if seg <= 0:
                    break

                build_tree_rec(n, -1 * turn, depth - 1)

        return
```

## Sistema desenvolvido

### Algoritmo *minimax*

- Alcançadas as posições terminais da árvore (*folhas*), propagam-se os escores a partir da **jogada**
	- Rodada do jogador: Turno **maximizador**
	- Rodada do oponente: Turno **minimizador**

## Sistema desenvolvido

### Poda $\alpha$/$\beta$

- Algoritmo **muito** efetivo para otimização da construção da árvore
- Através dela, saímos de um limite de profundidade de 6 *ply* para 50!
- Na maior$^*$ parte dos casos, a velocidade da construção é de alguns milissegundos

## Sistema desenvolvido

### Poda $\alpha$/$\beta$

![Algoritmo sobre o qual baseamos a nossa árvore](./assets/alpha_beta.png)

## Sistema desenvolvido

### Heurísticas de poda
- No entanto, algumas jogadas podem **demorar muito**

- Desenvolvemos heurística própria: limite no número de nós

- Contador atualizado em cada chamada recursiva

## Sistema desenvolvido

### Heurísticas de poda
```python
    # Variável de "segurança"
    seg = 10_000
```

$\quad\quad\quad\quad\quad\quad\quad\quad\quad\vdots$

```python
        # Atualiza o contador de segurança
        seg -= len(node.children)

        # Faz o passo de recursão para cada configuração
        for n in node.children:
```

$\quad\quad\quad\quad\quad\quad\quad\quad\quad\vdots$

```python
                # Encerra o loop pelo parâmetro de segurança
                if seg <= 0:
                    break
```

# Função de avaliação

## Termos de interesse

- Número de peças de cada campo
- Posição no tabuleiro
- Número de damas no tabuleiro

## Desenho geral

### Avaliação de escore (termos de quantidade)

```python
def calc_score (P):
    # Avalia posições das peças 'o'
    idx_v, idy_v   = np.where(P > 0.)
    numero_pecas_o = len(idx_v)

    # Termo associado a qtd de peças 'o' em P
    score += len(idx_v) * 100.

    # Termo associado a qtd de damas
    score += len(P > 1.) * 500.
```

## Desenho geral

### Avaliação de escore (termos posicionais)

```python
    # Itera por cada peça 'o'
    for idx, idy in zip(idx_v, idy_v):
        if np.abs(P[idx, idy]) < 2.:
            # Soma termo de proximidade com parede
            score += (1 - (0.5 / abs(idy - 5.5))) * 50.

            # Soma termo de proximidade com a borda
            score += idx * 10.
```

## Efeito e estilo de jogo

- Muitos dos termos da função de avaliação são proporcionais à **constantes**:
	- Dar mais ênfase para **capturas**
	- Dar mais ênfase na **criação de damas**
	- Privilegiar **posições defensivas** (mais próximas das paredes)

<!-- # Estudos de caso

## Estratégias puras

- Estratégias compostas pela tomada da **melhor** estratégia
- Movimentos escolhidos de forma **determinística**
- Serão apresentados os seguintes estudos de caso:
	1. Jogador aleatório contra agente inteligente
	1. Agente inteligente contra réplica
	1. Agentes inteligentes com diferentes funções de avaliação

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_0.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_1.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_2.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_3.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_4.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_5.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_6.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_7.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_8.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_9.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_10.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_11.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_12.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_13.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_14.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_15.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_16.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_17.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_18.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_19.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_20.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_21.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_22.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_23.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_24.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_25.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_26.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_27.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_28.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_29.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_30.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_31.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_32.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_33.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_34.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_35.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_36.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_37.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_38.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_39.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_40.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_41.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_42.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_43.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_44.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_44.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_45.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_46.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_47.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_48.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_49.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_50.png){ width=200px }

## Estratégias puras - Agente inteligente x Aleatório

\centering
![](./assets/int_vs_rand/f_51.png){ width=200px }

## Estratégias mistas

- Movimentos escolhidos de forma **aleatória**
- Probabilidades de jogada proporcionais ao escore
- Serão apresentados os seguintes estudos de caso:
	1. Jogador aleatório contra agente inteligente
	1. Agente inteligente contra réplica
	1. Agentes inteligentes com diferentes funções de avaliação

## Estratégias mistas

### Tomada de decisão para estratégia
```python
    # Estratégia mista (estocástica) com probabilidades
    # proporcionais aos escores.
    p_vec = np.array([nn.score for nn in tmp.children])
    p_vec[p_vec == np.inf]  = 1000.
    p_vec[p_vec == -np.inf] = -1000.
    p_vec[p_vec == np.nan]  = 0.

    p_vec -= np.min(p_vec)
    p_vec += 1.
    p_vec /= np.sum(p_vec)

    idx    = rand.choice(len(p_vec), p=p_vec)
    node   = deepcopy(tmp.children[idx])
```

# Conclusões

## Conclusões gerais

- Verificamos na prática o uso de um programa de certa forma análogo à *estratégia tipo A* proposta por Shannon em seu artigo sobre programas que jogam xadrez

- Percebeu-se o impacto que a função de custo tem sobre determinada estratégia.

- Verificamos o poder da pode $\alpha$/$\beta$ quando usada para busca em árvore 

- Foi percebida a necessidade de implementação de **heurísticas** para o controle no tempo de computação da árvore.

## Pontos de melhoria

- Para melhorar ainda mais as habilidades de nossa *engine*, sugere-se:
	1. Implementar uma melhor heurística de poda (a árvore fica **desbalanceada**)
	1. Adicionar termos de *proteção posicional* na função de avaliação
	1. Implementar uma interface que permita jogar contra!
	1. Permitir uma forma mais elegante de se alterar os hiper parâmetros da função de avaliação -->

##

\centering
\Large \textbf{Muito obrigado pela atenção!!}
